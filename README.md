# Huntington_2020_NatRevCancer

Scripts and pre-processed data associated with the manuscript:
Huntington, ND et al (2020). The Cancer - NK cell Immunity Cycle. Nature Reviews: Cancer.
doi: not-yet-known

Download pysingscore:
pip install git+https://github.com/DavisLaboratory/PySingscore


Process:

sample_to_cohort() --> panCancer_sample_group_dict.pickle

pan_cohort_immune_scores() --> panCancer_immune_score_df.pickle

pan_cohort_immune_assoc() --> panCancer_immune_assoc.pickle

pan_cohort_hazard_ratio() --> panCancer_survival_hazards.pickle