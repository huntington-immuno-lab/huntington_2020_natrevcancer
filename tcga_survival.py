# import GeneSetScoring
import singscore
from lifelines import CoxPHFitter
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patheffects as PathEffects
import numpy as np
import os
import pandas as pd
import pickle
import scipy.cluster.hierarchy as SciPyClus
# import seaborn as sns
import TCGAFunctions

#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#   tcga_survival.py
#       A python script which accompanies the manuscript:
#         Huntington et al. (2020). The Cancer - NK cell Immunity Cycle. Nature Reviews: Cancer.
#         doi: not-yet-known
#
#   #   #   #   #   #   #   #
#   Overview
#       This python script has been split into several classes and functions. Upon first execution the code should save
#       intermediate files, making subsequent runs much faster. Pre-processed intermediate files are available (see below,
#       or refer to the Git Repo README for further information).
#       - PathDir:
#           - contains all hard-coded paths; you will need to check this to ensure that it is correct as it is dependent
#               on your system configuration
#       - Process:
#           - immune_signatures() :: load/define immune cell signatures (gene sets) used in this study
#           - sample_to_cohort() ::
#           - pan_cohort_immune_scores
#           - pan_cohort_immune_assoc
#           - pan_cohort_hazard_ratio
#       - Plot:
#
#   #   #   #   #   #   #   #
#   Dependencies
#       As listed at the top of this script, a number of open source projects are dependencies for this work. The authors
#       would like to thank developers of these projects and suggest readers investigate the following links:
#       - lifelines
#       - matplotlib
#       - numpy
#       - pandas
#       - scitkit-learn
#       - scipy
#
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#   Data
#
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #



class PathDir:

    # This class handles the location of data which are processed by these scripts (strDataLoc) and the
    #  location of temporary processed files (strTempDataLoc)
    # NB: These folders need to be created by the user and if necessary the following declarations need to
    #  be edited. If not, the subsequent function will revert to using the location of this directory. The
    #  TCGA transcriptomic data are approximately 1.5GB; together with processed files this can take
    #  approximately N.N GB total.

    strTempDataLoc = 'C:\\temp\\huntington2020'

    # The NK gene signature is from Cursons et al (2019)
    #   DOI: 10.1158/2326-6066.CIR-18-0500
    strNKSigFile = '205802_2_supp_5410854_prn3pl.xlsx'

    strCurrentDir = os.getcwd()
    strDataLoc = os.path.join(strCurrentDir, 'data')
    strProcDataLoc = os.path.join(strCurrentDir, 'proc')

    def folder_checks(flagFailChecks=False):
        # This function checks that the required files/dependencies are present and
        #  if necessary prompts the user to download

        # if the user has downloaded/cloned this script from the GitLabs repo it should come with a data
        #  folder, but check anyway
        if not os.path.exists(PathDir.strDataLoc):
            # prompt the user and if permission is given, create the data folder
            strPromptResponse = raw_input('Create a directory at ' + PathDir.strDataLoc + ' (y/n)?')
            if strPromptResponse == 'y' or strPromptResponse == 'Y':
                os.mkdir(PathDir.strDataLoc)
            else:
                flagFailChecks=True
                print('Warning: no data folder found and user has prompted not to create; please \n' +
                      '\t\t\t create a data sub-folder in the same location as the tcga_survival.py script')

        return flagFailChecks

    def file_checks(flagFailChecks=False):
        # NK gene signature
        if not os.path.exists(os.path.join(PathDir.strDataLoc, PathDir.strNKSigFile)):
            print('warning: the NK cell signature file is not present; please download this file from the\n'+
                  '\t\t\tCancer Immunology Research Website (DOI: 10.1158/2326-6066.CIR-18-0500) or the \n'
                  '\t\t\tGitLab repository you downloaded this file (under the data folder)')

        # via the CIR website:
        # https://cancerimmunolres.aacrjournals.org/highwire/filestream/37099/field_highwire_adjunct_files/0/205802_2_supp_5410854_prn3pl.xlsx

        if not os.path.exists(strDataLoc):
            # use the directory of the script
            a=1

        return flagResult


class Process:

    # For this analysis we are focussing on overall survival (OS) but this list can be modified
    #  to also explore disease free interval (DFI), progression free interval (PFI) and
    #  disease-specific survival (DSS) as provided by:
    #       Liu et al (2018). Cell.
    #       doi: 10.1016/j.cell.2018.02.052
    listSurvTypes = ['OS']

    def immune_signatures(flagResult=False):
        # load/define immune cell signatures (gene sets) used in this study
        # Output:
        #   returns a dictionary of HGNC symbols used for gene set scoring

        # read in the NK signature from the Supplementary Table of Cursons, Guimaraes et al (2019)
        dfNKData = pd.read_excel(os.path.join(PathDir.strDataLoc, PathDir.strNKSigFile),
                                 sheet_name='NK_genes_curated',
                                 header=0, index_col=0, skiprows=1)
        listNKGenes = dfNKData[dfNKData['Cursons\nGuimaraes\nsigGene'] == True].index.tolist()

        # use a specified CD8 T cell signature
        listTCellGenes = ['CD8B', 'CD3D', 'CD3G', 'TRG',
                          'TRA', 'TRB', 'ICOS', 'UBASH3A',
                          'WNT7A', 'ST8SIA1']

        # use a cDC1 signature from Bottcher et al (2018), Cell.
        #  DOI: 10.1016/j.cell.2018.01.004
        listBottcherDCGenes = ['XCR1', 'CLEC9A', 'CLNK', 'BATF3']

        # FIX
        return {'NK cells': {'Up':listNKGenes},
                'CD8 T cells': {'Up':listTCellGenes},
                'Dendritic cells': {'Up':listBottcherDCGenes}}

    def sample_to_cohort(
            flagResult=False,
            strTempFile = 'panCancer_sample_group_dict.pickle',
            flagPerformExtraction=False):

        # explore transcript abundances only within primary tumour samples except for SKCM
        listTumourCodes = ['01', '03']
        listSKCMTumourCodes = ['06']

        dfMetaData = TCGAFunctions.PanCancer.extract_clinical_data()
        listCohorts = sorted(dfMetaData['type'].unique().tolist())

        if not os.path.exists(os.path.join(Process.strTempDataLoc, strTempFile)):
            flagPerformExtraction = True

        if flagPerformExtraction:
            print('Processing the RNA data to identify samples for downstream analyses')
            dfRNA = TCGAFunctions.PanCancer.extract_mess_rna()
            listRNAGenes = dfRNA.index.tolist()
            listRNAGenesClean = []
            for strGene in listRNAGenes:
                if strGene[0:len('?|')] == '?|':
                    listRNAGenesClean.append(strGene)
                else:
                    listRNAGenesClean.append(strGene.split('|')[0])
            dfRNA.rename(index=dict(zip(listRNAGenes, listRNAGenesClean)),
                         inplace=True)
            listRNASamples = dfRNA.columns.tolist()

            # check that the array is not all nan for some entries
            arrayRNADataAreGood = np.sum(dfRNA.isnull().values.astype(np.bool), axis=0) < 4000
            listRNADataAreGood = [listRNASamples[i] for i in np.where(arrayRNADataAreGood)[0]]

            dictCohortToOutputSamples = {}
            for strCohort in listCohorts:
                listPatientsInCohort = dfMetaData['bcr_patient_barcode'][
                    dfMetaData['type'] == strCohort].values.tolist()
                if strCohort == 'SKCM':
                    listRNASamplesMatchCohort = \
                        [strSample for strSample in listRNADataAreGood
                         if np.bitwise_and(strSample[0:len('TCGA-NN-NNNN')] in listPatientsInCohort,
                                           strSample[13:15] in listSKCMTumourCodes)]
                else:
                    listRNASamplesMatchCohort = \
                        [strSample for strSample in listRNADataAreGood
                         if np.bitwise_and(strSample[0:len('TCGA-NN-NNNN')] in listPatientsInCohort,
                                           strSample[13:15] in listTumourCodes)]

                dictCohortToOutputSamples[strCohort] = listRNASamplesMatchCohort

            with open(os.path.join(Process.strTempDataLoc, strTempFile), 'wb') as handFile:
                pickle.dump(dictCohortToOutputSamples, handFile, protocol=pickle.HIGHEST_PROTOCOL)

        else:

            with open(os.path.join(Process.strTempDataLoc, strTempFile), 'rb') as handFile:
                dictCohortToOutputSamples = pickle.load(handFile)

        return dictCohortToOutputSamples

    def pan_cohort_immune_scores(
            flagResult=False,
            strTempFile='panCancer_immune_score_df.pickle',
            flagPerformExtraction=False):

        dfMetaData = TCGAFunctions.PanCancer.extract_clinical_data()
        listCohorts = sorted(dfMetaData['type'].unique().tolist())

        dictImmuneSigs = Process.immune_signatures()
        listImmuneSigs = sorted(list(dictImmuneSigs.keys()))

        if not os.path.exists(os.path.join(Process.strTempDataLoc, strTempFile)):
            flagPerformExtraction = True

        if flagPerformExtraction:

            dfRNA = TCGAFunctions.PanCancer.extract_mess_rna(flagPerformExtraction=False)
            listRNAGenes = dfRNA.index.tolist()
            listRNAGenesClean = []
            for strGene in listRNAGenes:
                if strGene[0:len('?|')] == '?|':
                    listRNAGenesClean.append(strGene)
                else:
                    listRNAGenesClean.append(strGene.split('|')[0])
            dfRNA.rename(index=dict(zip(listRNAGenes, listRNAGenesClean)), inplace=True)
            listRNASamples = dfRNA.columns.tolist()

            # check that the array is not all nan for some entries
            arrayRNADataAreGood = np.sum(dfRNA.isnull().values.astype(np.bool), axis=0) < 4000
            listRNADataAreGood = [listRNASamples[i] for i in np.where(arrayRNADataAreGood)[0]]

            dfImmuneScores = pd.DataFrame(
                data=np.zeros((len(listRNADataAreGood), len(listImmuneSigs)),
                              dtype=np.float),
                index=listRNADataAreGood,
                columns=listImmuneSigs)

            print('Performing gene set scoring, this may take some time..')
            dictCohortToOutputSamples = Process.sample_to_cohort()
            for strCohort in listCohorts:
                print(f'\t\t .. scoring {strCohort} ({listCohorts.index(strCohort)+1}/{len(listCohorts)})')

                listRNASamplesMatchCohort = dictCohortToOutputSamples[strCohort]

                arrayProgSteps = np.linspace(start=(len(listRNASamplesMatchCohort)*0.1)-1,
                                             stop=len(listRNASamplesMatchCohort)-1,
                                             num=10)

                numProgStep = 0
                for strSample in listRNASamplesMatchCohort:

                    listNonNaNGenes = \
                        dfRNA[~np.isnan(dfRNA[strSample].values.astype(np.float))].index.tolist()

                    arraySampleData = dfRNA[strSample].reindex(listNonNaNGenes).values.astype(np.float)

                    dfImmuneScores.loc[strSample, listImmuneSigs] = \
                        GeneSetScoring.FromInput.single_sample_rank_multiple_scores(
                            listAllGenes=listNonNaNGenes,
                            arrayTranscriptAbundance=arraySampleData,
                            listSignatures=listImmuneSigs,
                            dictSignatures=dictImmuneSigs,
                            numLogLevel=0,
                            flagApplyNorm=True
                        )

                    if listRNASamplesMatchCohort.index(strSample) >= arrayProgSteps[numProgStep]:
                        print(f'\t\t\t\t {(numProgStep+1)*10}% complete for this cohort..')
                        numProgStep += 1

            dfImmuneScores.to_pickle(os.path.join(Process.strTempDataLoc, strTempFile))

        else:

            dfImmuneScores = pd.read_pickle(os.path.join(Process.strTempDataLoc, strTempFile))

        return dfImmuneScores

    def pan_cohort_immune_assoc(
            flagResult=False,
            strTempFile = 'panCancer_immune_assoc.pickle',
            flagPerformExtraction=False):

        dfMetaData = TCGAFunctions.PanCancer.extract_clinical_data()
        listCohorts = sorted(dfMetaData['type'].unique().tolist())

        dictImmuneSigs = Process.immune_signatures()
        listImmuneSigs = sorted(list(dictImmuneSigs.keys()))

        listSigCombos = []
        for iSigOne in range(len(listImmuneSigs)-1):
            strSigOne = listImmuneSigs[iSigOne]
            for iSigTwo in range(iSigOne+1,len(listImmuneSigs)):
                strSigTwo = listImmuneSigs[iSigTwo]
                listSigCombos.append(f'{strSigOne} vs {strSigTwo}')

        dfImmuneComparisonOut = pd.DataFrame(
            data=np.zeros((len(listCohorts), len(listSigCombos)),
                          dtype=np.float),
            index=listCohorts,
            columns=listSigCombos)

        if not os.path.exists(os.path.join(Process.strTempDataLoc, strTempFile)):
            flagPerformExtraction = True

        if flagPerformExtraction:
            dfImmuneScores = Process.pan_cohort_immune_scores(flagPerformExtraction=False)

            print('Calculating associations across the cohorts..')
            dictCohortToOutputSamples = Process.sample_to_cohort()

            for strCohort in listCohorts:
                print(f'\t\t .. for {strCohort} ({listCohorts.index(strCohort) + 1}/{len(listCohorts)})')

                listRNASamplesMatchCohort = dictCohortToOutputSamples[strCohort]

                dfImmuneAssocForCohort = dfImmuneScores[listImmuneSigs].reindex(listRNASamplesMatchCohort).corr()

                for iSigOne in range(len(listImmuneSigs) - 1):
                    strSigOne = listImmuneSigs[iSigOne]
                    for iSigTwo in range(iSigOne + 1, len(listImmuneSigs)):
                        strSigTwo = listImmuneSigs[iSigTwo]

                        dfImmuneComparisonOut.loc[strCohort, f'{strSigOne} vs {strSigTwo}'] = \
                            dfImmuneAssocForCohort.loc[strSigOne, strSigTwo]

            dfImmuneComparisonOut.to_pickle(os.path.join(Process.strTempDataLoc, strTempFile))

        else:

            dfImmuneComparisonOut = pd.read_pickle(os.path.join(Process.strTempDataLoc, strTempFile))

        return dfImmuneComparisonOut

    def pan_cohort_hazard_ratio(
            flagResult=False,
            strTempFile='panCancer_survival_hazards.pickle',
            flagPerformExtraction=False):

        # explore transcript abundances only within primary tumour samples except for SKCM
        listTumourCodes = ['01', '03']
        listSKCMTumourCodes = ['06']

        listOfListsGenesToPlot = [Plot.listLigands,
                                  Plot.listActivatingCytokines,
                                  Plot.listReceptors,
                                  Plot.listSecretedChemokines]

        listGenesToCheck = []
        for iList in range(len(listOfListsGenesToPlot)):
            listGenesToCheck = listGenesToCheck + listOfListsGenesToPlot[iList]

        dfMetaData = TCGAFunctions.PanCancer.extract_clinical_data()
        listUniqueCohorts = sorted(dfMetaData['type'].unique().tolist())

        listCohorts = ['LAML', 'DLBC']
        for strCohort in listUniqueCohorts:
            if strCohort not in listCohorts:
                listCohorts.append(strCohort)

        if not os.path.exists(os.path.join(Process.strTempDataLoc, strTempFile)):
            flagPerformExtraction = True

        if flagPerformExtraction:

            dfRNA = TCGAFunctions.PanCancer.extract_mess_rna(flagPerformExtraction=False)
            listRNAGenes = dfRNA.index.tolist()
            listRNAGenesClean = []
            for strGene in listRNAGenes:
                if strGene[0:len('?|')] == '?|':
                    listRNAGenesClean.append(strGene)
                else:
                    listRNAGenesClean.append(strGene.split('|')[0])
            dfRNA.rename(index=dict(zip(listRNAGenes, listRNAGenesClean)), inplace=True)
            listRNASamples = dfRNA.columns.tolist()

            # check that the array is not all nan for some entries
            arrayRNADataAreGood = np.sum(dfRNA.isnull().values.astype(np.bool), axis=0) < 4000
            listRNADataAreGood = [listRNASamples[i] for i in np.where(arrayRNADataAreGood)[0]]

            listOfListsSamplesInCohort = []
            listFlatCohorts = []
            for strCohort in listCohorts:
                listPatientsInCohort = dfMetaData['bcr_patient_barcode'][
                    dfMetaData['type'] == strCohort].values.tolist()
                if strCohort == 'SKCM':
                    listRNASamplesMatchCohort = \
                        [strSample for strSample in listRNADataAreGood
                         if np.bitwise_and(strSample[0:len('TCGA-NN-NNNN')] in listPatientsInCohort,
                                           strSample[13:15] in listSKCMTumourCodes)]
                else:
                    listRNASamplesMatchCohort = \
                        [strSample for strSample in listRNADataAreGood
                         if np.bitwise_and(strSample[0:len('TCGA-NN-NNNN')] in listPatientsInCohort,
                                           strSample[13:15] in listTumourCodes)]

                listOfListsSamplesInCohort.append(listRNASamplesMatchCohort)
                listFlatCohorts += [strCohort] * len(listRNASamplesMatchCohort)

            listFlatSamplesInCohort = [strSample for listSamples in listOfListsSamplesInCohort for strSample in
                                       listSamples]

            dfForPlot = dfRNA[listFlatSamplesInCohort].reindex(listGenesToCheck).transpose().copy(deep=True)
            dfForPlot['cohort'] = pd.Series(listFlatCohorts, index=listFlatSamplesInCohort)

            listPropHazardFeatures = ['Mean', 'p-value']
            listOutputFeatures = []
            for strSurv in Process.listSurvTypes:
                for strGene in listGenesToCheck:
                    for strFeature in listPropHazardFeatures:
                        listOutputFeatures.append(strGene + '::' + strSurv + '::' + strFeature)

            dfSurvData = pd.DataFrame(data=np.zeros((len(listCohorts), len(listOutputFeatures)), dtype=np.float),
                                      index=listCohorts,
                                      columns=listOutputFeatures)

            for strSurv in Process.listSurvTypes:

                arraySurvDataAreGood = \
                    np.bitwise_and(np.bitwise_and(
                        dfMetaData[strSurv].notnull().values.astype(np.bool),
                        dfMetaData[strSurv + '.time'].notnull().values.astype(np.bool)),
                        dfMetaData['age_at_initial_pathologic_diagnosis'].notnull().values.astype(np.bool))
                listSurvDataAreGood = \
                    dfMetaData['bcr_patient_barcode'].iloc[np.where(arraySurvDataAreGood)[0]].values.tolist()

                for strGene in listGenesToCheck:

                    for iCohort in range(len(listCohorts)):
                        strCohort = listCohorts[iCohort]
                        print('.. ' + strCohort)
                        listCohortSamples = dfForPlot[dfForPlot['cohort'] == strCohort].index.tolist()
                        listCohortSamplesGoodSurv = \
                            [strSample for strSample in listCohortSamples
                             if strSample[0:len('TCGA-NN-NNNN')] in listSurvDataAreGood]

                        listOutputSamplesPatientBarcode = [strSample[0:len('TCGA-NN-NNNN')] for strSample in
                                                           listCohortSamplesGoodSurv]

                        structPropHazard = CoxPHFitter()
                        if len(listOutputSamplesPatientBarcode) == len(set(listOutputSamplesPatientBarcode)):
                            # proceed
                            listSamplesForSurv = listCohortSamplesGoodSurv
                            dfForCoxPH = pd.DataFrame(data=np.zeros((len(listSamplesForSurv), 1), dtype=np.float),
                                                      index=[strSample[0:len('TCGA-NN-NNNN')] for strSample in
                                                             listSamplesForSurv],
                                                      columns=[strGene])
                            dfForCoxPH.loc[:, strGene] = np.nan_to_num(
                                dfRNA[listSamplesForSurv].loc[strGene].values.astype(np.float))
                        else:
                            # do something with replicate samples
                            listPatientHasObs = []
                            listPatientHasMultiObs = []
                            for strSample in listCohortSamplesGoodSurv:
                                if strSample[0:len('TCGA-NN-NNNN')] not in listPatientHasObs:
                                    listPatientHasObs.append(strSample[0:len('TCGA-NN-NNNN')])
                                else:
                                    listPatientHasMultiObs.append(strSample[0:len('TCGA-NN-NNNN')])

                            listSamplesForSurv = []
                            for strSample in listCohortSamplesGoodSurv:
                                if strSample[0:len('TCGA-NN-NNNN')] not in listPatientHasMultiObs:
                                    listSamplesForSurv.append(strSample)
                            dfForCoxPH = pd.DataFrame(data=np.zeros((len(listSamplesForSurv), 1), dtype=np.float),
                                                      index=[strSample[0:len('TCGA-NN-NNNN')] for strSample in
                                                             listSamplesForSurv],
                                                      columns=[strGene])
                            dfForCoxPH.loc[:, strGene] = np.nan_to_num(
                                dfRNA[listSamplesForSurv].loc[strGene].values.astype(np.float))

                            print(
                                'WARNING: ' + strCohort + ' has multiple samples per patient that need to be resolved')

                        listSurvTime = []
                        listSurvFlag = []
                        listPatientAge = []
                        for strPatient in dfForCoxPH.index.tolist():
                            listSurvTime.append(
                                dfMetaData[strSurv + '.time'][
                                    dfMetaData['bcr_patient_barcode'] == strPatient].values.astype(np.float)[
                                    0] / 365.25)
                            listSurvFlag.append(
                                dfMetaData[strSurv][dfMetaData['bcr_patient_barcode'] == strPatient].values.astype(
                                    np.bool)[0])
                            listPatientAge.append(
                                dfMetaData['age_at_initial_pathologic_diagnosis'][
                                    dfMetaData['bcr_patient_barcode'] == strPatient].values.astype(np.float)[0])

                        dfForCoxPH['Age'] = pd.Series(listPatientAge, index=dfForCoxPH.index.tolist())
                        dfForCoxPH['surv_time'] = pd.Series(listSurvTime, index=dfForCoxPH.index.tolist())
                        dfForCoxPH['death_event'] = pd.Series(listSurvFlag, index=dfForCoxPH.index.tolist())

                        if np.ptp(dfForCoxPH[strGene]) > 0:
                            structPropHazard.fit(dfForCoxPH, duration_col='surv_time',
                                                 event_col='death_event', step_size=0.5)

                            dfSurvData.loc[strCohort, strGene + '::' + strSurv + '::Mean'] = \
                                structPropHazard.summary.loc[strGene, 'coef']

                            dfSurvData.loc[strCohort, strGene + '::' + strSurv + '::p-value'] = \
                                structPropHazard._compute_p_values()[0]

            dfSurvData.to_pickle(os.path.join(Process.strTempDataLoc, strTempFile))

        else:

            dfSurvData = pd.read_pickle(os.path.join(Process.strTempDataLoc, strTempFile))

        return dfSurvData

class Plot:

    strOutFolder = 'C:\\Users\\jcur0014\\OneDrive - Monash University\\papers\\2020_Huntington_NK_Cell_review\\plots'
    numFontSize = 7

    # Create a dictionary to map from official HGNC symbols to gene/protein synonyms which may be more recognisable
    #   for readers with an immunology background.
    dictGeneNames = {'ADORA2A': 'ADORA2A (A2AR)',
                     'ENTPD1': 'CD39 (ENTPD1)',
                     'KLRK1': 'KLRK1 (NKG2D)',
                     'CD244': 'CD244 (2B4)',
                     'CD226': 'CD226 (DNAM1)',
                     'NT5E': 'NT5E (CD73)',
                     'ICAM1': 'ICAM1 (CD54)',
                     'TMIGD2': 'TMIGD2 (CD28H)',
                     'KLRC1': 'KLRC1 (NKG2A)',
                     'KLRC2': 'KLRC2 (NKG2C)',
                     'RAET1E': 'RAET1E (ULBP4)',
                     'RAET1G': 'RAET1G (ULBP5)',
                     'RAET1L': 'RAET1L (ULBP6)',
                     'PVR': 'PVR (CD155)',
                     'PVRL2': 'PVRL2 (NECTIN2/CD112)',
                     'HHLA2': 'HHLA2 (B7-H7)',
                     'DKFZp686O24166': 'NCR3LG1 (B7-H6)',
                     'CD274': 'CD274 (PD-L1)'
                     }

    listReceptors = ['ADORA2A', 'CD2', 'CD226', 'CD244',
                     'TIGIT', 'TMIGD2', 'NCR1', 'NCR2', 'NCR3',
                     'NT5E', 'ITGAL', 'ITGB2', 'PDCD1',
                     'KLRC1', 'KLRC2', 'KLRC3', 'KLRC4', 'KLRD1',
                     'KLRF1', 'KLRG1', 'KLRG2', 'KLRK1',
                     'KIR2DL1', 'KIR2DL3', 'KIR2DL4', 'KIR2DS4',
                     'KIR3DL1', 'KIR3DL2', 'KIR3DL3']

    # Ligands which bind to a variety of activating and inhibitory receptors on NK cells as outlined in
    #  the corresponding manuscript
    listLigands = ['ENTPD1', 'CD48', 'CD58', 'CD274',
                   'DKFZp686O24166', 'HHLA2', 'HLA-C',
                   'HLA-DPB1', 'HLA-E', 'ICAM1',
                   'MICA', 'MICB', 'PVR', 'PVRL2',
                   'ULBP1', 'ULBP2', 'ULBP3',
                   'RAET1E', 'RAET1G', 'RAET1L']

    # Important cytokines for the proliferation and maintenance of NK cells (and T cells) including
    #   IL-12 (a heterodimer formed from IL12A/B expression products; often provided by other
    #   immune cells such as DCs), IL-15, and IL-18
    listActivatingCytokines = ['IL12A', 'IL12B', 'IL15', 'IL18']

    # Chemokines secreted by NK cells to support cDC1 cells (FLT3 ligand, XCL1/2) and other immune cells
    #  (RANTES/CCL5)
    listSecretedChemokines = ['CCL5', 'FLT3LG', 'XCL1', 'XCL2']

    listCohortsToDrop = ['ESCA', 'COAD', 'PAAD', 'PCPG', 'UCS', 'OV', 'LUSC', 'STAD']

    def pan_cohort_hazard_heatmap(flagResult=False):

        listSigCorrOfInt = ['Dendritic cells vs NK cells',
                            'CD8 T cells vs NK cells',
                            'CD8 T cells vs Dendritic cells']
        listSigCorrDisp = ['$r_{P}$(cDC1; NK)',
                           '$r_{P}$(NK; CD8$^{+}$ T)',
                           '$r_{P}$(cDC1; CD8$^{+}$ T)']

        listOfListsGenesToPlot = [Plot.listLigands,
                                  Plot.listActivatingCytokines,
                                  Plot.listReceptors,
                                  Plot.listSecretedChemokines]
        listGroupLabels = ['Tumor\nligands',
                           'Activating\ncytokines',
                           'NK cell\nreceptors',
                           'NK secreted\ncytokines']

        dfCorrData = Process.pan_cohort_immune_assoc()

        listGenesToPlot = []
        listGroupLabelYPos = []
        listThickHorizLinePos = []
        listThinHorizLinePos = []
        for iList in range(len(listOfListsGenesToPlot)):
            listGroupLabelYPos.append(len(listGenesToPlot) + (len(listOfListsGenesToPlot[iList]) / 2) - 0.5)
            if iList > 0:
                listThickHorizLinePos.append(len(listGenesToPlot) - 0.5)
            listThinHorizLinePos = listThinHorizLinePos + \
                                   [i for i in np.arange(start=len(listGenesToPlot) + 3.5,
                                                         stop=len(listGenesToPlot) +
                                                              len(listOfListsGenesToPlot[iList]) - 1.5,
                                                         step=4)]
            listGenesToPlot = listGenesToPlot + listOfListsGenesToPlot[iList]

        arrayThickHorizLinePos = np.array(listThickHorizLinePos, dtype=np.float)
        arrayThinHorizLinePos = np.array(listThinHorizLinePos, dtype=np.float)
        arrayGeneGroupLabelPositions = np.array(
            listGroupLabelYPos, dtype=np.float)

        dfSurvData = Process.pan_cohort_hazard_ratio()

        for strSurv in Process.listSurvTypes:

            listOutCohorts = [strCohort for strCohort in dfSurvData.index.tolist()
                              if strCohort not in Plot.listCohortsToDrop]

            listMeanCols = [strCol for strCol in dfSurvData
                            if np.bitwise_and(strSurv in strCol, '::Mean' in strCol)]

            listMeanColsToPlot = [strCol for strCol in listMeanCols
                                  if strCol.partition('::')[0] in listGenesToPlot]

            numMaxAbsVal = 1.0

            arrayLink = \
                SciPyClus.linkage(dfSurvData[listMeanColsToPlot].reindex(listOutCohorts),
                                  method='single')
            arrayHRClustOrder = SciPyClus.leaves_list(arrayLink)
            listCohortClust = [listOutCohorts[i] for i in arrayHRClustOrder][::-1]

            handFig = plt.figure(figsize=(6,5))

            handAx = handFig.add_axes([0.24, 0.15, 0.72, 0.75])
            structAxPos = handAx.get_position()
            handHM = handAx.matshow(
                dfSurvData[listMeanColsToPlot].reindex(listCohortClust).transpose(),
                cmap=plt.cm.PRGn_r,
                vmin=-numMaxAbsVal,
                vmax=numMaxAbsVal,
                aspect='auto')

            numTests = 1
            for iYPos in arrayThinHorizLinePos:
                handAx.axhline(y=iYPos,
                               xmin=0.0, xmax=1.0,
                               color='0.75',
                               linestyle='--',
                               linewidth=0.5)
            for iYPos in arrayThickHorizLinePos:
                handAx.axhline(y=iYPos,
                               xmin=0.0, xmax=1.0,
                               color='w',
                               linewidth=1.5)
                handAx.axhline(y=iYPos,
                               xmin=0.0, xmax=1.0,
                               color='k',
                               linewidth=1.0)

            for iCohort in range(len(listCohortClust)):
                strCohort = listCohortClust[iCohort]
                for iGene in range(len(listGenesToPlot)):
                    strGene = listGenesToPlot[iGene]
                    numPVal = dfSurvData[strGene + '::' + strSurv + '::p-value'].loc[strCohort].astype(np.float)

                    numAdjPVal = numTests*numPVal
                    if np.bitwise_or(np.isnan(numPVal), numPVal > 1.0):
                        numAdjPVal = 1

                    if np.bitwise_and(numAdjPVal > 0.01, numAdjPVal <= 0.10):
                        handAx.scatter(x=np.float(iCohort), y=np.float(iGene),
                                       marker='o',
                                       s=10,
                                       facecolors='none',
                                       edgecolors='k',
                                       linewidths=0.5,
                                       alpha=1.0,
                                       path_effects=[PathEffects.withStroke(linewidth=1.0, foreground='w')]
                                       )
                    elif numAdjPVal <= 0.01:
                        handAx.scatter(x=np.float(iCohort), y=np.float(iGene),
                                       marker='*',
                                       s=15,
                                       facecolors='none',
                                       edgecolors='k',
                                       linewidths=0.5,
                                       alpha=1.0,
                                       path_effects=[PathEffects.withStroke(linewidth=1.0, foreground='w')])
                    elif numAdjPVal > 0.10:
                        handAx.scatter(x=np.float(iCohort), y=np.float(iGene),
                                       marker='X',
                                       s=5,
                                       c='k',
                                       lw=0.25)

            for iGroupLabel in range(len(listGroupLabels)):
                handAx.text(-6, arrayGeneGroupLabelPositions[iGroupLabel],
                            listGroupLabels[iGroupLabel], rotation=0,
                            fontsize=Plot.numFontSize*0.7,
                            fontweight='bold',
                            ha='center', va='center')

            handAx.set_xticks([])
            handAx.set_yticks([])

            for iCohort in range(len(listCohortClust)):
                handAx.text(iCohort, len(listGenesToPlot)+2.5,
                            listCohortClust[iCohort], rotation=90,
                            fontsize=Plot.numFontSize*0.8,
                            ha='center', va='center')
            handFig.text(structAxPos.x0-0.015*structAxPos.width,
                         structAxPos.y0-0.06*structAxPos.height,
                         'TCGA cohort',
                         ha='right', va='bottom',
                         fontsize=Plot.numFontSize, fontweight='bold')

            for iGene in range(len(listGenesToPlot)):
                if listGenesToPlot[iGene] in Plot.dictGeneNames.keys():
                    handAx.text(-0.7, iGene,
                                Plot.dictGeneNames[listGenesToPlot[iGene]], rotation=0,
                                fontsize=Plot.numFontSize*0.7, style='italic',
                                ha='right', va='center')
                else:
                    handAx.text(-0.7, iGene,
                                listGenesToPlot[iGene], rotation=0,
                                fontsize=Plot.numFontSize*0.7, style='italic',
                                ha='right', va='center')

            handHMCMapAx = handFig.add_axes([0.30, 0.94, 0.20, 0.03])
            handAbundColorBar = handFig.colorbar(handHM,
                                                 cax=handHMCMapAx,
                                                 ticks=[-numMaxAbsVal, 0, numMaxAbsVal],
                                                 format='%02.1f',
                                                 orientation='horizontal',
                                                 extend='both')
            handAbundColorBar.ax.tick_params(labelsize=Plot.numFontSize * 0.8)
            handHMCMapAx.xaxis.set_label_position('top')
            handHMCMapAx.set_xlabel('$log_{e}$(Hazard Ratio)', fontsize=Plot.numFontSize * 0.8)
            structAxPos = handHMCMapAx.get_position()

            # Add figure annotation; negative ln(HR) associated with better survival
            handFig.text(x=structAxPos.x0-0.25*structAxPos.width,
                         y=structAxPos.y0+0.6*structAxPos.height,
                         s='Higher expression\nassociated with\nbetter patient\nsurvival',
                         ha='center', va='center',
                         fontstyle='italic',
                         fontsize=Plot.numFontSize * 0.6)

            # Add figure annotation; positive ln(HR) associated with better survival
            handFig.text(x=structAxPos.x0+1.25*structAxPos.width,
                         y=structAxPos.y0+0.6*structAxPos.height,
                         s='Higher expression\nassociated with\nworse patient\nsurvival',
                         ha='center', va='center',
                         fontstyle='italic',
                         fontsize=Plot.numFontSize * 0.6)

            handPValLegend = handFig.add_axes([0.70, 0.94, 0.20, 0.02])
            handPValLegend.scatter(x=0.0, y=0.0,
                                   marker='X',
                                   s=7,
                                   c='k',
                                   alpha=1.0)
            handPValLegend.scatter(x=1.0, y=0.0,
                                   marker='o',
                                   s=10,
                                   facecolors='none',
                                   edgecolors='k',
                                   linewidths=0.5,
                                   alpha=1.0,
                                       path_effects=[PathEffects.withStroke(linewidth=1.0, foreground='w')])
            handPValLegend.scatter(x=2.0, y=0.0,
                                   marker='*',
                                   s=15,
                                   facecolors='none',
                                   edgecolors='k',
                                   linewidths=0.5,
                                   alpha=1.0,
                                       path_effects=[PathEffects.withStroke(linewidth=1.0, foreground='w')])

            handPValLegend.set_xlim([-0.2, 2.2])
            handPValLegend.set_yticks([])
            handPValLegend.xaxis.set_ticks_position('top')
            handPValLegend.xaxis.set_label_position('bottom')
            handPValLegend.set_xlabel('$p$-value', fontsize=Plot.numFontSize*0.6)
            handPValLegend.set_xticks([0, 1, 2])
            handPValLegend.set_xticklabels(['> 0.10', '0.01-0.10', '< 0.01'], fontsize=Plot.numFontSize*0.6)

            handAx = handFig.add_axes([0.24, 0.01, 0.72, 0.06])
            handCorrHM = handAx.matshow(
                dfCorrData[listSigCorrOfInt].reindex(listCohortClust).transpose(),
                cmap=plt.cm.BrBG,
                vmin=-1.0,
                vmax=1.0,
                aspect='auto')
            handAx.set_xticks([])
            handAx.set_yticks([])
            for iComp in range(len(listSigCorrDisp)):
                handAx.text(-1, iComp, listSigCorrDisp[iComp],
                            ha='right', va='center',
                            fontsize=Plot.numFontSize*0.7)


            handCorrHMCMapAx = handFig.add_axes([0.05, 0.01, 0.015, 0.08])
            handAbundColorBar = handFig.colorbar(handCorrHM,
                                                 cax=handCorrHMCMapAx,
                                                 ticks=[-1, 0, 1],
                                                 format='%02.1f',
                                                 orientation='vertical')
            handAbundColorBar.ax.tick_params(labelsize=Plot.numFontSize * 0.8)
            structAxPos = handCorrHMCMapAx.get_position()
            handFig.text(structAxPos.x0-structAxPos.width,
                         structAxPos.y0 + 0.5*structAxPos.height,
                         'Pearson\'s\ncorrelation',
                         ha='center', va='center',
                         fontsize=Plot.numFontSize * 0.8,
                         rotation=90)

            matplotlib.rcParams['pdf.fonttype'] = 42
            handFig.savefig(os.path.join(Plot.strOutFolder, strSurv+'SelCohort_HazRatio_HeatMap.png'),
                            ext='png', dpi=300)
            handFig.savefig(os.path.join(Plot.strOutFolder, strSurv+'SelCohort_HazRatio_HeatMap.pdf'),
                            ext='pdf', dpi=600)
            plt.close(handFig)

        return flagResult


# _ = Process.pan_cohort_immune_scores(flagPerformExtraction=False)
# _ = Process.pan_cohort_immune_assoc(flagPerformExtraction=False)

# _ = Process.immune_signatures()

_ = Plot.pan_cohort_hazard_heatmap()
